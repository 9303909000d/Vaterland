
state={
	id=196
	name="STATE_196"
	resources={
		chromium=15.000
	}

	history={
		owner = UKR
		buildings = {
			infrastructure = 5
			air_base = 3

		}
		add_core_of = UKR
		add_core_of = UKR
		1942.11.21 = {
			owner = RUK
			controller = RUK

		}
		2000.1.1 = {
			owner = UKR

		}

	}

	provinces={
		568 721 737 767 3573 6771 9712 11715 
	}
	manpower=728363
	buildings_max_level_factor=1.000
	state_category=rural
}
