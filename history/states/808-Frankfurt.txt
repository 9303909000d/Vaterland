
state={
	id=808
	name="STATE_808"

	history={
		owner = GER
		victory_points = {
			6488 20.0 
		}
		buildings = {
			infrastructure = 8
			arms_factory = 1
			industrial_complex = 1

		}
		add_core_of = GER
		1939.1.1 = {
			buildings = {
				air_base = 3
				arms_factory = 3

			}

		}
		set_demilitarized_zone = no
		1936.3.7 = {
			set_demilitarized_zone = no

		}
		1946.1.1 = {
			owner = USA
			controller = USA

		}
		1950.1.1 = {
			owner = FRG

		}

	}

	provinces={
		589 3574 6444 6488 9486 
	}
	manpower=3258765
	buildings_max_level_factor=1.000
	state_category=city
}
