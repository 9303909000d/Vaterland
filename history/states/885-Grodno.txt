
state={
	id=885
	name="STATE_885"

	history={
		owner = BLR
		victory_points = {
			3393 3
		}
		buildings = {
			infrastructure = 5
			air_base = 3

		}
		add_core_of = BLR
		add_core_of = BLR
		1940.5.10 = {
			owner = BLR
			controller = BLR
			set_state_name = Hrodna
			set_province_name = { id = 3393 name = "Hrodna" }

		}
		1942.11.21 = {
			owner = BLR
			controller = GER

		}
		1944.6.20 = {
			owner = BLR
			controller = GER

		}
		1946.1.1 = {
			owner = BLR
			controller = BLR

		}
		2000.1.1 = {
			owner = BLR

		}

	}

	provinces={
		318 3256 3281 3393 12904 13721 
	}
	manpower=757200
	buildings_max_level_factor=1.000
	state_category=rural
}
