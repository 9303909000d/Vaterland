
state={
	id=91
	name="STATE_91"
	resources={
		oil=6.000
		steel=8.000
	}

	history={
		owner = UKR
		victory_points = {
			11479 5 
		}
		buildings = {
			infrastructure = 6
			industrial_complex = 2
			air_base = 5

		}
		add_core_of = POL
		add_core_of = UKR
		1940.5.10 = {
			owner = SOV
			controller = SOV
			set_state_name = Lviv
			set_province_name = {
				id = 11479
				name = Lviv

			}

		}
		1942.11.21 = {
			owner = GGP
			controller = GGP

		}
		1944.6.20 = {
			controller = SOV
			GER = {
				set_province_controller = 11479
				set_province_controller = 9558
				set_province_controller = 536
				set_province_controller = 9468

			}

		}
		1946.1.1 = {
			owner = SOV
			controller = SOV

		}
		2000.1.1 = {
			owner = UKR

		}

	}

	provinces={
		536 9468 9558 11479 
	}
	manpower=3227800
	buildings_max_level_factor=1.000
	state_category=city
}
