focus_tree = {
	id = Italy
	country = {
		factor = 0
		modifier = {
			add = 10
			tag = ITA
		}
	}
	focus = {
		id = ITA_victory_in_africa
		icon = victory_in_africa_shine
		x = 10
		y = 2
		cost = 45
		available_if_capitulated = yes
		search_filters = { FOCUS_FILTER_POLITICAL } 
		ai_will_do = {
			factor = 1
		}
		available = {
			has_war = no
			owns_state = 271
		}
		completion_reward = {
			add_ideas= ITA_victory_in_africa
		}

	}
	focus = {
		id = ITA_reclaim_trentino
		icon = GFX_ITA_Reclaim_Trentino
		x = 9
		y = 3
		cost = 70
		available_if_capitulated = yes
		prerequisite = { focus = ITA_victory_in_africa }
		search_filters = { FOCUS_FILTER_POLITICAL } 
		ai_will_do = {
			factor = 1
		}
		completion_reward = {
			add_state_claim= 769
			add_state_claim= 39
		}
	}

	focus = {
		id = ITA_speech_of_trentino__south_tyrol
		icon = GFX_ITA_Trentino_and_Sudtirol
		x = 11
		y = 3
		cost = 70
		available_if_capitulated = yes
		prerequisite = { focus = ITA_victory_in_africa }
		search_filters = { FOCUS_FILTER_WAR_SUPPORT } 
		ai_will_do = {
			factor = 1
		}
		completion_reward = {
			add_war_support= 0.05
		}
	}

	focus = {
		id = ITA_the_tyrol_war
		icon = GFX_ITA_Tyrol_War
		x = 10
		y = 4
		cost = 70
		available_if_capitulated = yes
		prerequisite = { 
			focus = ITA_reclaim_trentino 
		}
		prerequisite = { 
			focus = ITA_speech_of_trentino__south_tyrol 
		}
		search_filters = { FOCUS_FILTER_ANNEXATION } 
		ai_will_do = {
			factor = 1
		}
		available = {
			has_army_manpower{ size > 350000 }
		}
		completion_reward = {
			create_wargoal= { 
			type = annex_everything 
			target = AUS
			}
		}
	}
 }