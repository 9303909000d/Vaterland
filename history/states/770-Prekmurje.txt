state={
	id=770
	name="STATE_770"

	provinces={
		6672
	}
	history={
		owner = AUS
		buildings = {
			infrastructure = 6

		}
		add_core_of = AUS
		1941.6.22 = {
			owner = AUS
			controller = AUS
		}
		1946.1.1 = {
			owner = AUS
			controller = AUS
		}	
		add_core_of = SLV
		2000.1.1 = {
			owner = SLV
		}	
	}

	manpower=205120
	buildings_max_level_factor=1.000
	state_category = rural
}
