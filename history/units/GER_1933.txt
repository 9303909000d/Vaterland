﻿##############################################
## German Templates
##############################################
division_template = {
	name = "Infanterie-Division"			 #Germany might be overpowered, but since they demobilized, they focus on quality, not quantity.		
	division_names_group = GER_Inf_01

	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }

   		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }

  		infantry = { x = 2 y = 0 }
		infantry = { x = 2 y = 1 }
		infantry = { x = 2 y = 2 }

	}
	support = {
			recon = { x = 0 y = 0 }
			engineer = { x = 0 y = 1 }
			artillery = { x = 0 y = 2 }
	}
}
division_template = {
	name = "Königliche Wache Division" #I had some fun with this. These are elite Royal Guard Divisions			
	division_names_group = GER_ROYAL_01

	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }

   		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }

  		infantry = { x = 2 y = 0 }
		infantry = { x = 2 y = 1 }
		infantry = { x = 2 y = 2 }
		artillery_brigade = { x = 2 y = 3 }

		medium_armor = { x = 3 y = 0 }

	}
	support = {
			recon = { x = 0 y = 0 }
			engineer = { x = 0 y = 1 }
			signal_company = { x = 0 y = 2 }
	}
	priority = 2
}


division_template = {
		name = "Sicherungs-Division"         # Garrison
		regiments = {
			garrison = { x = 0 y = 0 }
			garrison = { x = 0 y = 1 }
			garrison = { x = 0 y = 2 }
			garrison = { x = 1 y = 0 }
			garrison = { x = 1 y = 1 }
			garrison = { x = 1 y = 2 }
		}
		priority = 0
	}
division_template = {
	name = "Panzer-Division"
	
	division_names_group = GER_Arm_01
	regiments = {
		medium_armor = { x = 0 y = 0 } #In 1933, these are all light tanks
		medium_armor = { x = 0 y = 1 }
		medium_armor = { x = 1 y = 0 }
		medium_armor = { x = 1 y = 1 }
		motorized = { x = 2 y = 0 }
		motorized = { x = 2 y = 1 }
	}
	support = {
		recon = { x = 0 y = 0 }
		engineer = { x = 0 y = 1 }
		artillery = { x = 0 y = 2 }
	}
}
division_template = {
	name = "Infanterie-Division (mot.)"
	
	division_names_group = GER_MOT_01
	regiments = {
		
		motorized = { x = 0 y = 0 }
		motorized = { x = 0 y = 1 }
		motorized = { x = 0 y = 2 }
		motorized = { x = 1 y = 0 }
		motorized = { x = 1 y = 1 }
		motorized = { x = 1 y = 2 }
	}
	support = {
		recon = { x = 0 y = 0 }
		engineer = { x = 0 y = 1 }
		artillery = { x = 0 y = 2 }
	}
}
division_template = {
	name = "Gebirgs-Brigade"
	
	division_names_group = GER_Mnt_01
	regiments = {
		mountaineers = { x = 0 y = 0 }
		mountaineers = { x = 0 y = 1 }
		mountaineers = { x = 0 y = 2 }
		mountaineers = { x = 1 y = 0 }
		mountaineers = { x = 1 y = 1 }
		mountaineers = { x = 1 y = 2 }
		artillery_brigade = { x = 2 y = 0 }
	}
	recon = { x= 0 y = 0 }
	engineer = { x = 0 y = 1 }
}
division_template = {
	name = "Kavallerie-Brigade"
	
	division_names_group = GER_Cav_01
	regiments = {
		cavalry = { x = 0 y = 0 }
		cavalry = { x = 0 y = 1 }
		cavalry = { x = 1 y = 0 }
		cavalry = { x = 1 y = 1 }
	}
	support = {
		recon = { x = 0 y = 0 }
	}
}
##############################################
## German Land Units -51 Divisions
##############################################
units = {
	
	# Königliche Wache Heersgruppe (The Royal Guard Army)
	
	division= { # 1. Königliche Wache Div.
		division_name = {
			is_name_ordered = yes
			name_order = 1
		}
		location = 6521 #Berlin
		division_template = "Königliche Wache Division"
		start_experience_factor = 0.4
	}

	division= { # 2. Königliche Wache Div.
		division_name = {
			is_name_ordered = yes
			name_order = 2
		}
		location = 6521 #Berlin
		division_template = "Königliche Wache Division"
		start_experience_factor = 0.4
	}
	
	division= { # 3. Königliche Wache Div.
		division_name = {
			is_name_ordered = yes
			name_order = 3
		}
		location = 6521 #Berlin
		division_template = "Königliche Wache Division"
		start_experience_factor = 0.4
	}

	division= { # 4. Königliche Wache Div.
		division_name = {
			is_name_ordered = yes
			name_order = 4
		}
		location = 9347 #Hamburg
		division_template = "Königliche Wache Division"
		start_experience_factor = 0.4
	}

	division= { # 5. Königliche Wache Div.
		division_name = {
			is_name_ordered = yes
			name_order = 5
		}
		location = 6488 #Frankfurt
		division_template = "Königliche Wache Division"
		start_experience_factor = 0.4
	}

	division= { # 6. Königliche Wache Div.
		division_name = {
			is_name_ordered = yes
			name_order = 6
		}
		location = 6332 #Koenigsberg
		division_template = "Königliche Wache Division"
		start_experience_factor = 0.4
	}

	division= { # 7. Königliche Wache Div.
		division_name = {
			is_name_ordered = yes
			name_order = 7
		}
		location = 9570 #Breslau
		division_template = "Königliche Wache Division"
		start_experience_factor = 0.4
	}

	#Heeresgruppe Deutschland - Army Group 1# (Garrisons for major cities)

	division= { # 1. Infanterie Div.
		division_name = {
			is_name_ordered = yes
			name_order = 1
		}
		location = 6521 #Berlin
		division_template = "Infanterie-Division"
		start_experience_factor = 0.3
	}

	division= { # 2. Infanterie Div.
		division_name = {
			is_name_ordered = yes
			name_order = 2
		}
		location = 3535 #
		division_template = "Infanterie-Division"
		start_experience_factor = 0.3
	}

	division= { # 3. Infanterie Div.
		division_name = {
			is_name_ordered = yes
			name_order = 3
		}
		location = 9517 #Stuttgart
		division_template = "Infanterie-Division"
		start_experience_factor = 0.3
	}

	division= { # 4. Infanterie Div.
		division_name = {
			is_name_ordered = yes
			name_order = 4
		}
		location = 241 #Wilhemshaven
		division_template = "Infanterie-Division"
		start_experience_factor = 0.3
	}

	division= { # 5. Infanterie Div.
		division_name = {
			is_name_ordered = yes
			name_order = 5
		}
		location = 321 #Rostock
		division_template = "Infanterie-Division"
		start_experience_factor = 0.3
	}

	division= { # 6. Infanterie Div.
		division_name = {
			is_name_ordered = yes
			name_order = 6
		}
		location = 6282 #Stettin
		division_template = "Infanterie-Division"
		start_experience_factor = 0.3
	}

	division= { # 7. Infanterie Div.
		division_name = {
			is_name_ordered = yes
			name_order = 7
		}
		location = 6558 #Poznan
		division_template = "Infanterie-Division"
		start_experience_factor = 0.3
	}

	division= { # 8. Infanterie Div.
		division_name = {
			is_name_ordered = yes
			name_order = 8
		}
		location = 362 #Danzig
		division_template = "Infanterie-Division"
		start_experience_factor = 0.3
	}

	#Heeresgruppe Westen# - Army Group West (French border)

	division= { # 17. Infanterie Div.
		division_name = {
			is_name_ordered = yes
			name_order = 17
		}
		location = 678 #Mulhouse
		division_template = "Infanterie-Division"
		start_experience_factor = 0.3
	}

	division= { # 23. Infanterie Div.
		division_name = {
			is_name_ordered = yes
			name_order = 23
		}
		location = 6529 #Alsace
		division_template = "Infanterie-Division"
		start_experience_factor = 0.2
	}

	division= { # 9. Infanterie Div.
		division_name = {
			is_name_ordered = yes
			name_order = 9
		}
		location = 9503 #Strasbourg
		division_template = "Infanterie-Division"
		start_experience_factor = 0.3
	}

	division= { # 13. Infanterie Div.
		division_name = {
			is_name_ordered = yes
			name_order = 13
		}
		location = 1346 #Alsace
		division_template = "Infanterie-Division"
		start_experience_factor = 0.3
	}

	division= { # 19. Infanterie Div.
		division_name = {
			is_name_ordered = yes
			name_order = 19
		}
		location = 3629 #Moselle
		division_template = "Infanterie-Division"
		start_experience_factor = 0.2
	}

	division= { # 10. Infanterie Div.
		division_name = {
			is_name_ordered = yes
			name_order = 10
		}
		location = 11502 #Moselle
		division_template = "Infanterie-Division"
		start_experience_factor = 0.3
	}

	division= { # 11. Infanterie Div.
		division_name = {
			is_name_ordered = yes
			name_order = 11
		}
		location = 9559 #Metz
		division_template = "Infanterie-Division"
		start_experience_factor = 0.3
	}

	division= { # 21. Infanterie Div.
		division_name = {
			is_name_ordered = yes
			name_order = 21
		}
		location = 9559 #Metz
		division_template = "Infanterie-Division"
		start_experience_factor = 0.2
	}

	#Heeresgruppe Ost# - Army Group East (Poland)

	division= { # 12. Infanterie Div.
		division_name = {
			is_name_ordered = yes
			name_order = 12
		}
		location = 6464 #?
		division_template = "Infanterie-Division"
		start_experience_factor = 0.3
	}

	division= { # 28. Infanterie Div.
		division_name = {
			is_name_ordered = yes
			name_order = 28
		}
		location = 506 #?
		division_template = "Infanterie-Division"
		start_experience_factor = 0.3
	}

	division= { # . Infanterie Div.
		division_name = {
			is_name_ordered = yes
			name_order = 
		}
		location = 479 #
		division_template = "Infanterie-Division"
		start_experience_factor = 0.3
	}

	division= { # . Infanterie Div.
		division_name = {
			is_name_ordered = yes
			name_order = 
		}
		location = 11558 #
		division_template = "Infanterie-Division"
		start_experience_factor = 0.3
	}

	division= { # 20. Infanterie Div.
		division_name = {
			is_name_ordered = yes
			name_order = 20
		}
		location = 9532 #
		division_template = "Infanterie-Division"
		start_experience_factor = 0.3
	}

	division= { # . Infanterie Div.
		division_name = {
			is_name_ordered = yes
			name_order = 
		}
		location = 17 #
		division_template = "Infanterie-Division"
		start_experience_factor = 0.3
	}

	division= { # 29. Infanterie Div.
		division_name = {
			is_name_ordered = yes
			name_order = 29
		}
		location = 3381 #Gniezno
		division_template = "Infanterie-Division"
		start_experience_factor = 0.2
		start_equipment_factor = 0.6 #Lack of equipment because Germany is mobilising more men, in response to the risk of war
	}

	division= { # 18. Infanterie Div.
		division_name = {
			is_name_ordered = yes
			name_order = 18
		}
		location = 3381 #Gniezno
		division_template = "Infanterie-Division"
		start_experience_factor = 0.3
	}

	division= { # 25. Infanterie Div.
		division_name = {
			is_name_ordered = yes
			name_order = 
		}
		location = 243 #
		division_template = "Infanterie-Division"
		start_experience_factor = 0.3
	}

	division= { # 24. Infanterie Div.
		division_name = {
			is_name_ordered = yes
			name_order = 
		}
		location = 279 #
		division_template = "Infanterie-Division"
		start_experience_factor = 0.3
	}

	division= { # 22. Infanterie Div.
		division_name = {
			is_name_ordered = yes
			name_order = 
		}
		location = 3259 #
		division_template = "Infanterie-Division"
		start_experience_factor = 0.3
	}

	division= { # 26. Infanterie Div.
		division_name = {
			is_name_ordered = yes
			name_order = 26
		}
		location = 6347 #
		division_template = "Infanterie-Division"
		start_experience_factor = 0.3
		start_equipment_factor = 0.8 #Lack of equipment because Germany is mobilising more men, in response to the risk of war
	}

	division= { # 14. Infanterie Div.
		division_name = {
			is_name_ordered = yes
			name_order = 14
		}
		location = 9346 #
		division_template = "Infanterie-Division"
		start_experience_factor = 0.3
	}

	division= { # 27. Infanterie Div.
		division_name = {
			is_name_ordered = yes
			name_order = 27
		}
		location = 6375 #Allenstein
		division_template = "Infanterie-Division"
		start_experience_factor = 0.2
		start_equipment_factor = 0.65 #Lack of equipment because Germany is mobilising more men, in response to the risk of war
	}

	division= { # 15. Infanterie Div.
		division_name = {
			is_name_ordered = yes
			name_order = 
		}
		location = 11386 #
		division_template = "Infanterie-Division"
		start_experience_factor = 0.3
	}

	division= { # 16. Infanterie Div.
		division_name = {
			is_name_ordered = yes
			name_order = 
		}
		location = 3384 #Gumbinnen
		division_template = "Infanterie-Division"
		start_experience_factor = 0.3
	}

	division= { # 30. Infanterie Div.
		division_name = {
			is_name_ordered = yes
			name_order = 30
		}
		location = 3288 #Klaipeda
		division_template = "Infanterie-Division"
		start_experience_factor = 0.2
		start_equipment_factor = 0.7 #Lack of equipment because Germany is mobilising more men, in response to the risk of war
	}

	#Heeresgruppe Sud# - Army Group South (Austria and Hungary)

	division= { # 1. Gebirgs Brig.
		division_name = {
			is_name_ordered = yes
			name_order = 1
		}
		location = 707 #Austria
		division_template = "Gebirgs-Brigade"
		start_experience_factor = 0.3
	}

	division= { # 2. Gebirgs Brig.
		division_name = {
			is_name_ordered = yes
			name_order = 2
		}
		location = 11620 #Austria
		division_template = "Gebirgs-Brigade"
		start_experience_factor = 0.3
	}

	division= { # 3. Gebirgs. Brig.
		division_name = {
			is_name_ordered = yes
			name_order = 3
		}
		location = 3485 #Czech
		division_template = "Gebirgs-Brigade"
		start_experience_factor = 0.3
	}

	division= { # 31. Infanterie Div.
		division_name = {
			is_name_ordered = yes
			name_order = 31
		}
		location = 11638 #Austria
		division_template = "Infanterie-Division"
		start_experience_factor = 0.3
	}

	division= { # 32. Infanterie Div.
		division_name = {
			is_name_ordered = yes
			name_order = 32
		}
		location = 6512 #Oppeln
		division_template = "Infanterie-Division"
		start_experience_factor = 0.3
		start_equipment_factor = 0.6 #Lack of equipment because Germany is mobilising more men, in response to the risk of war
	}

	division= { # 33. Infanterie Div.
		division_name = {
			is_name_ordered = yes
			name_order = 33
		}
		location = 9471 #Chemnitz
		division_template = "Infanterie-Division"
		start_experience_factor = 0.2
		start_equipment_factor = 0.7 #Lack of equipment because Germany is mobilising more men, in response to the risk of war
	}

	division= { # 34. Infanterie Div.
		division_name = {
			is_name_ordered = yes
			name_order = 34
		}
		location = 9441 #Zwickau
		division_template = "Infanterie-Division"
		start_experience_factor = 0.2
	}

	division= { # 35. Infanterie Div.
		division_name = {
			is_name_ordered = yes
			name_order = 35
		}
		location = 3571 #Czech Border
		division_template = "Infanterie-Division"
		start_experience_factor = 0.2
		start_equipment_factor = 0.8 #Lack of equipment because Germany is mobilising more men, in response to the risk of war
	}

	#Heimatverteidigung# - Homeland Defence (The crappy garrisons)

	division= { #1. Sicherungs Div.
		division_name = {
			is_name_ordered = yes
			name_order = 1
		}
		location = 9522 #Aachen
		division_template = "Sicherungs-Division"
		start_experience_factor = 0.1 #Crappies
		start_equipment_factor = 0.7 #Lack of equipment because Germany is mobilising more men, in response to the risk of war
	}

	division= { #2. Sicherungs. Div.
		division_name = {
			is_name_ordered = yes
			name_order = 2
		}
		location = 3512 #Belgian Border
		division_template = "Sicherungs-Division"
		start_experience_factor = 0.1
		start_equipment_factor = 0.6 #Lack of equipment because Germany is mobilising more men, in response to the risk of war
	}

	division= { #3. Sicherungs. Div
		division_name = {
			is_name_ordered = yes
			name_order = 3
		}
		location = 11360 #Emden
		division_template = "Sicherungs-Division"
		start_experience_factor = 0.2
		start_equipment_factor = 0.7 #Lack of equipment because Germany is mobilising more men, in response to the risk of war
	}

	division= { #4. Sicherungs. Div
		division_name = {
			is_name_ordered = yes
			name_order = 4
		}
		location = 6389 #Kiel
		division_template = "Sicherungs-Division"
		start_experience_factor = 0.2
		start_equipment_factor = 0.7 #Lack of equipment because Germany is mobilising more men, in response to the risk of war
	}

	division= { #5. Sicherungs. Div
		division_name = {
			is_name_ordered = yes
			name_order = 5
		}
		location = 11560 #Mainz
		division_template = "Sicherungs-Division"
		start_experience_factor = 0.2
		start_equipment_factor = 0.6 #Lack of equipment because Germany is mobilising more men, in response to the risk of war
	}

	division= { #6. Sicherungs. Div
		division_name = {
			is_name_ordered = yes
			name_order = 6
		}
		location = 11544 #Nuremberg
		division_template = "Sicherungs-Division"
		start_experience_factor = 0.2
		start_equipment_factor = 0.7 #Lack of equipment because Germany is mobilising more men, in response to the risk of war
	}

	division= { #7. Sicherungs. Div
		division_name = {
			is_name_ordered = yes
			name_order = 7
		}
		location = 6282 #Stettin
		division_template = "Sicherungs-Division"
		start_experience_factor = 0.2
		start_equipment_factor = 0.7 #Lack of equipment because Germany is mobilising more men, in response to the risk of war
	}

	division= { #8. Sicherungs. Div
		division_name = {
			is_name_ordered = yes
			name_order = 8
		}
		location = 3522 #Magdeburg
		division_template = "Sicherungs-Division"
		start_experience_factor = 0.1
		start_equipment_factor = 0.6 #Lack of equipment because Germany is mobilising more men, in response to the risk of war
	}

	division= { #9. Sicherungs. Div
		division_name = {
			is_name_ordered = yes
			name_order = 9
		}
		location = 6332 #Koenigsberg
		division_template = "Sicherungs-Division"
		start_experience_factor = 0.2
		start_equipment_factor = 0.7 #Lack of equipment because Germany is mobilising more men, in response to the risk of war
	}

	division= { #10. Sicherungs. Div
		division_name = {
			is_name_ordered = yes
			name_order = 10
		}
		location = 6521 #Berlin
		division_template = "Sicherungs-Division"
		start_experience_factor = 0.1
		start_equipment_factor = 0.5 #Lack of equipment because Germany is mobilising more men, in response to the risk of war
	}

	#Heeresgruppe Kavallerie# - Army Group Cavalry

	division= { #1. Kavallerie. Div
		division_name = {
			is_name_ordered = yes
			name_order = 1
		}
		location = 514 #Dresden
		division_template = "Kavallerie-Brigade"
		start_experience_factor = 0.2
		start_equipment_factor = 0.8 #Lack of equipment because Germany is mobilising more men, in response to the risk of war
	}

	division= { #2. Kavallerie. Div
		division_name = {
			is_name_ordered = yes
			name_order = 2
		}
		location = 587 #Cologne
		division_template = "Kavallerie-Brigade"
		start_experience_factor = 0.2
		start_equipment_factor = 0.7 #Lack of equipment because Germany is mobilising more men, in response to the risk of war
	}

	#Panzerkorps Berlin# - Panzercorps of Berlin(newly formed)

	division= { #1st Panzer Division
		division_name = {
			is_name_ordered = yes
			name_order = 1
		}
		location = 6521 #Berlin
		division_template = "Panzer-Division"
		force_equipment_variants = { Light_Tank_1934 = { owner = GER } } #Pz I
		start_experience_factor = 0.3
	}
	division= { #2nd Panzer Division
		division_name = {
			is_name_ordered = yes
			name_order = 2
		}
		location = 6521 #Berlin
		division_template = "Panzer-Division"
		force_equipment_variants = { Light_Tank_1934 = { owner = GER } } #Pz I
		start_experience_factor = 0.3
	}
	division= { #3rd Panzer Division
		division_name = {
			is_name_ordered = yes
			name_order = 3
		}
		location = 6521 #Berlin
		division_template = "Panzer-Division"
		force_equipment_variants = { Light_Tank_1934 = { owner = GER } } #Pz I
		start_experience_factor = 0.3
		start_equipment_factor = 0.7
	}
	division= { #1st Inf. Div. (mot.)
		division_name = {
			is_name_ordered = yes
			name_order = 1
		}
		location = 6521 #Berlin
		division_template = "Infanterie-Division (mot.)"
		start_experience_factor = 0.3
	}
	division= { #2nd Inf. Div. (mot.)
		division_name = {
			is_name_ordered = yes
			name_order = 2
		}
		location = 6521 #Berlin
		division_template = "Infanterie-Division (mot.)"
		start_experience_factor = 0.3
	}


	





}
	
		

##############################################
## Naval OOB
##############################################
units = {
	fleet = {
		name = "Kriegsmarine"
		naval_base = 241 # Wilhemshaven
		task_force = {
			name = "Schwere Marineflotte Ein"
			location = 241 # Wilhemshaven
			
			ship = { name = "Schlesien" definition = battleship equipment = { BB_equipment_1895 = { amount = 1 owner = GER } } }
			ship = { name = "Schleswig-Holstein" definition = battleship equipment = { BB_equipment_1900 = { amount = 1 owner = GER } } }
			ship = { name = "Hamburg" definition = battleship equipment = { BB_equipment_1900 = { amount = 1 owner = GER } } }
			ship = { name = "Barbarossa" definition = battleship equipment = { BB_equipment_1916 = { amount = 1 owner = GER } } }
			ship = { name = "Frankfurt" definition = battleship equipment = { BB_equipment_1922 = { amount = 1 owner = GER } } }
			ship = { name = "Munich" definition = battleship equipment = { BB_equipment_1922 = { amount = 1 owner = GER } } }
			ship = { name = "Kaiser" pride_of_the_fleet = yes definition = battleship equipment = { BB_equipment_1933 = { amount = 1 owner = GER } } }
			ship = { name = "Frederick" definition = battleship equipment = { BB_equipment_1933 = { amount = 1 owner = GER } } }


			ship = { name = "Hessen" definition = light_cruiser equipment = { CL_equipment_1922 = { amount = 1 owner = GER } } }			
			ship = { name = "Danzig" definition = light_cruiser equipment = { CL_equipment_1922 = { amount = 1 owner = GER } } }
			ship = { name = "Königsberg" definition = light_cruiser equipment = { CL_equipment_1922 = { amount = 1 owner = GER } } }
			ship = { name = "Karlsruhe" definition = light_cruiser equipment = { CL_equipment_1922 = { amount = 1 owner = GER } } }
			ship = { name = "Köln" definition = light_cruiser equipment = { CL_equipment_1922 = { amount = 1 owner = GER } } }
			ship = { name = "Emden" definition = light_cruiser equipment = { CL_equipment_1922 = { amount = 1 owner = GER } } }
			ship = { name = "Berlin" definition = light_cruiser equipment = { CL_equipment_1933 = { amount = 1 owner = GER } } }
			ship = { name = "Leipzig" definition = light_cruiser equipment = { CL_equipment_1933 = { amount = 1 owner = GER } } }

			ship = { name = "Hannover" definition = heavy_cruiser equipment = { CA_equipment_1906 = { amount = 1 owner = GER } } }
			ship = { name = "Reich" definition = heavy_cruiser equipment = { CA_equipment_1906 = { amount = 1 owner = GER } } }
			ship = { name = "Prussia" definition = heavy_cruiser equipment = { CA_equipment_1922 = { amount = 1 owner = GER } } }
			ship = { name = "Dresden" definition = heavy_cruiser equipment = { CL_equipment_1922 = { amount = 1 owner = GER } } }
			ship = { name = "Wilhelm II" definition = heavy_cruiser equipment = { CL_equipment_1933 = { amount = 1 owner = GER } } }
			
			
		}
		task_force = {
			name = "Schwere Marineflotte Zwei"
			location = 241 # Wilhelmshaven

			ship = { name = "Stolz" definition = battlecruiser equipment = { BC_equipment_1916 = { amount = 1 owner = GER } } }
			ship = { name = "Bismarck" definition = battlecruiser equipment = { BC_equipment_1916 = { amount = 1 owner = GER } } }
			ship = { name = "Sieg" definition = battlecruiser equipment = { BC_equipment_1933 = { amount = 1 owner = GER } } }

			ship = { name = "Endsieg" definition = carrier equipment = { CV_equipment_1930 = { amount = 1 owner = GER } } }
			ship = { name = "Deutschland" definition = carrier equipment = { CV_equipment_1930 = { amount = 1 owner = GER } } }
		}
		task_force = {
			name = "Leichte Marineflotte"
			location = 241 # Wilhemshaven

			ship = { name = "1. Torpedobootsflottille" definition = destroyer equipment = { DD_equipment_1900 = { amount = 1 owner = GER } } }
			ship = { name = "2. Torpedobootsflottille" definition = destroyer equipment = { DD_equipment_1912 = { amount = 1 owner = GER } } }
			ship = { name = "3. Torpedobootsflottille" definition = destroyer equipment = { DD_equipment_1912 = { amount = 1 owner = GER } } }
			ship = { name = "4. Torpedobootsflottille" definition = destroyer equipment = { DD_equipment_1916 = { amount = 1 owner = GER } } }
			ship = { name = "5. Torpedobootsflottille" definition = destroyer equipment = { DD_equipment_1916 = { amount = 1 owner = GER } } }
			ship = { name = "6. Torpedobootsflottille" definition = destroyer equipment = { DD_equipment_1916 = { amount = 1 owner = GER } } }
			ship = { name = "7. Torpedobootsflottille" definition = destroyer equipment = { DD_equipment_1922 = { amount = 1 owner = GER } } }
			ship = { name = "8. Torpedobootsflottille" definition = destroyer equipment = { DD_equipment_1922 = { amount = 1 owner = GER } } }
			ship = { name = "9. Torpedobootsflottille" definition = destroyer equipment = { DD_equipment_1922 = { amount = 1 owner = GER } } }
			ship = { name = "10. Torpedobootsflottille" definition = destroyer equipment = { DD_equipment_1922 = { amount = 1 owner = GER } } }
			ship = { name = "11. Torpedobootsflottille" definition = destroyer equipment = { DD_equipment_1922 = { amount = 1 owner = GER } } }
			ship = { name = "12. Torpedobootsflottille" definition = destroyer equipment = { DD_equipment_1933 = { amount = 1 owner = GER } } }
			
		}
	}	
}
			
##############################################
## German Air Units
##############################################
air_wings = {	
	### Kampffliegerschule - Berlin
	64 = { 
		# Kampffliegerschule (Fokker D.XIII)	
		Fighter_equipment_1933 =  {
			owner = "GER" 
			amount = 50
		}
		name = "Kampffliegerschule"	
	}
	### Deutsche Luft Hansa - Berlin
	64 = { 
		# Blitzstrecken	(Do 11)	
		Tactical_Bomber_equipment_1933 = {
			owner = "GER" 
			amount = 60
		}
		name = Blitzstrecken
		# Deutsche Luft Hansa (Ju W 34)
		transport_plane_equipment_1 = {
			owner = "GER" 
			amount = 4 
		}
		name = "Luft Hansa"		
	}
}
##############################################
## German Production
##############################################
instant_effect = {
	# Kar 98k
	add_equipment_production = {
		equipment = {
			type = Small_Arms_equipment_1900
			creator = "GER"
		}
		requested_factories = 10
		progress = 0.1
		efficiency = 50
	}
	# Support Equipment
	add_equipment_production = {
		equipment = {
			type = support_equipment_1
			creator = "GER"
		}
		requested_factories = 2
		progress = 0.3
		efficiency = 50
	}
	# leFH18
	add_equipment_production = {
		equipment = {
			type = artillery_equipment_1
			creator = "GER"
		}
		requested_factories = 1
		progress = 0.3
		efficiency = 50
	}
	# Pz IIs
	add_equipment_production = {
		equipment = {
			type = Light_Tank_equipment_1934
			creator = "GER"
			version_name = "PzKpfw. I Ausf. C"
		}
		requested_factories = 2
		progress = 0.4
		efficiency = 50
	}
	# Opel Blitz
	add_equipment_production = {
		equipment = {
			type = truck_equipment_1936
			creator = "GER"
		}
		requested_factories = 2
		progress = 0.4
		efficiency = 50
	}
	# Bf-109s
	add_equipment_production = {
		equipment = {
			type = Fighter_equipment_1936
			creator = "GER"
		}
		requested_factories = 1
		progress = 0.15
		efficiency = 50
	}
	# Ju-87s
	add_equipment_production = {
		equipment = {
			type = CAS_equipment_1933
			creator = "GER"
		}
		requested_factories = 1
		progress = 0.2
		efficiency = 50
	}
	# He-111s
	add_equipment_production = {
		equipment = {
			type = Tactical_Bomber_equipment_1933
			creator = "GER"
		}
		requested_factories = 1
		progress = 0.1
		efficiency = 50
	}
	
	#########################################################################
	#  Ships Under Contruction
	#########################################################################	
	## The Formula used is X=100-(100Y)/T ; X= Progress, Y= Time Left, T= Total Building time
}
