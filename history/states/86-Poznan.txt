
state={
	id=86
	name="STATE_86"

	history={
		owner = GER
		victory_points = {
			6558 5 
		}
		victory_points = {
			279 3 
		}
		victory_points = {
			3381 1
		}
		buildings = {
			infrastructure = 6
			industrial_complex = 1
			11232 = {
				bunker = 1

			}
			9532 = {
				bunker = 1

			}
			3381 = {
				bunker = 1

			}
			air_base = 3

		}
		add_core_of = GER
		1940.5.10 = {
			owner = GER
			controller = GER

		}
		1938.3.12 = {
			add_claim_by = GER

		}
		1944.6.20 = {
			owner = GER
			controller = GER

		}
		1946.1.1 = {
			owner = GER
			controller = GER

		}

	}

	provinces={
		17 243 279 388 3381 3460 3532 6558 9532 11232 11558 
	}
	manpower=2106500
	buildings_max_level_factor=1.000
	state_category=city
}
