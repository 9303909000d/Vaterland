
state={
	id=766
	name="STATE_766"
	resources={
		steel=8.000
	}

	history={
		owner = GER
		victory_points = {
			6464 3
		}
		buildings = {
			infrastructure = 6
			industrial_complex = 2

		}
		add_core_of = GER
		1940.5.10 = {
			owner = GER
			controller = GER
		}
		1944.6.20 = {
			owner = GER
			controller = GER
		}
		1946.1.1 = {
			owner = GER
			controller = GER
		}
	}

	provinces={
		506 6464 
	}
	manpower=1056542
	buildings_max_level_factor=1.000
	state_category=city
}
