﻿#########################################################################
# Poland - 1933
#########################################################################
1933.1.1 = {
	capital = 10
	oob = "POL_1933"
	set_research_slots = 3
	set_convoys = 10
	#######################
	# Research
	#######################
	set_technology = {
		Small_Arms_1916 = 1
		Small_Arms_1935 = 1
		truck_1936 = 1
		tech_mountaineers = 1
		tech_support = 1		
		tech_engineers = 1
		tech_recon = 1
		gw_artillery = 1
		interwar_antiair = 1
		
		Fighter_1933 = 1
		Tactical_Bomber_1933 = 1
		CAS_1936 = 1
	# Armour Tech

		Heavy_Tank_1916 = 1

		Heavy_Tank_1917 = 1
		Light_Tank_1926 = 1
	#Naval Stuff
		DD_1885 = 1
		DD_1900 = 1
		DD_1912 = 1
		DD_1916 = 1
		DD_1922 = 1
		DD_1933 = 1	

		CA_1885 = 1
		CA_1895 = 1
		
		SS_1895 = 1
		SS_1912 = 1
		SS_1916 = 1
		SS_1922 = 1
	#
		Trench_Warfare = 1
	}
	#######################
	# Politics
	#######################
	set_politics = {
		ruling_party = authoritarian
		last_election = "1930.11.23"
		election_frequency = 36
		elections_allowed = yes
	}
	set_popularities = {
		fascist = 3
		authoritarian = 50
		democratic = 25
		socialist = 20
		communist = 2
	}	
	add_ideas = {
        # Spirit
	POL_Sons_of_Poland
	POL_Something_to_fight_for	
        # Cabinet
		
        POL_AM_Jan_Pilsudski
        POL_MoS_Bronislaw_Piernacki
		# POL_HoI_Janusz_Jagrym_Maleszewski
        # Military Staff
		POL_CoStaff_Janusz_Gasiorowski
		POL_CoArmy_Jozef_Pilsudski
        POL_CoNavy_Jerzy_Swirski
        POL_CoAir_Ludomil_Rayski
	}
	#######################
	# Leaders
	#######################
	# Fascism
	create_country_leader = {
		name = "Jan Mosdorf"
		desc = ""
		picture = "P_F_Jan_Mosdorf.tga"
		expire = "1965.1.1"
		ideology = falangism
		traits = { POSITION_President SUBIDEOLOGY_Falangism HoS_Barking_Buffoon }
	}
	# Paternal Autocracy
	create_country_leader = {
		name = "August Czartoryski"
		desc = ""
		picture = "P_A_August_Czartoryski.tga"
		expire = "1965.1.1"
		ideology = monarchism
		traits = { POSITION_monarchy SUBIDEOLOGY_monarchism HoS_Stern_Imperialist }
	}
	# Democracy
	create_country_leader = {
		name = "Roman Dmowski"
		desc = ""
		picture = "P_D_Roman_Dmowski.tga"
		expire = "1965.1.1"
		ideology = social_conservatism
		traits = { POSITION_President SUBIDEOLOGY_Conservatism HoS_Weary_Stiff_Neck }
	}
	# Socialism
	create_country_leader = {
		name = "Ignacy Daszynski"
		desc = ""
		picture = "P_S_Ignacy_Daszynski.tga"
		expire = "1965.1.1"
		ideology = socialism
		traits = { POSITION_General_Secretary SUBIDEOLOGY_Socialism HoS_Popular_Figurehead }
	}
	# Marxism-Leninism
	create_country_leader = {
		name = "Boleslaw Bierut"
		desc = ""
		picture = "P_C_Boleslaw_Bierut.tga"
		expire = "1965.1.1"
		ideology = stalinism
		traits = { POSITION_General_Secretary SUBIDEOLOGY_Stalinism HoS_Barking_Buffoon }
	}
	#######################
	# Generals
	#######################
	create_field_marshal = {
		name = "Józef Piłsudski"
		picture = "M_Josef_Pilsudski_MILITARY.tga"
		traits = { media_personality offensive_doctrine inspirational_leader }
		id = 3150
		skill = 2
		attack_skill = 3
		defense_skill = 2
		planning_skill = 1
		logistics_skill = 1
	}

	create_corps_commander = {
		name = "Kazimierz Sosnkowski"
		picture = "M_Kazimierz_Sosnowski.tga"
		traits = { old_guard organizer }
		id = 3151
		skill = 3
		attack_skill = 2
		defense_skill = 3
		planning_skill = 3
		logistics_skill = 4
    }

    create_corps_commander = {
		name = "Edward Rydz-Śmigły"
		picture = "M_Edward_Rydz_Smigly.tga"
		traits = {  politically_connected war_hero media_personality skilled_staffer  }
		id = 3152
		skill = 2
		attack_skill = 3
		defense_skill = 2
		planning_skill = 1
		logistics_skill = 2
    }

    create_corps_commander = {
		name = "Leon Berbecki"
		picture = "M_Leon_Berbecki.tga"
		traits = {  old_guard infantry_officer politically_connected }
		id = 3153
		skill = 2
		attack_skill = 2
		defense_skill = 3
		planning_skill = 2
		logistics_skill = 2
    }

    create_corps_commander = {
		name = "Jan Kruszewski"
		picture = "M_Jan_Kruszewski.tga"
		traits = {  politically_connected infantry_officer }
		id = 3154
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 1
    }
    
    create_corps_commander = {
		name = "Juliusz Rómmel"
		picture = "M_Juliusz_Rommel.tga"
		traits = {  old_guard war_hero trait_cautious cavalry_officer cavalry_leader }
		id = 3155
		skill = 2
		attack_skill = 4
		defense_skill = 1
		planning_skill = 2
		logistics_skill = 1
    }
    
    create_corps_commander = {
		name = "Tadeusz Piskor"
		picture = "M_Tadeusz_Piskor.tga"
		traits = {  infantry_officer trait_engineer }
		id = 3156
		skill = 2
		attack_skill = 1
		defense_skill = 2
		planning_skill = 3
		logistics_skill = 3
    }
    
    create_corps_commander = {
		name = "Stefan Dąb-Biernacki"
		picture = "M_Stefan_Dab_Biernacki.tga"
		traits = {  old_guard politically_connected harsh_leader substance_abuser }
		id = 3157
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 1
		logistics_skill = 1
    }
    
    create_corps_commander = {
		name = "Kazimierz Fabrycy"
		picture = "M_Kazimierz_Fabrycy.tga"
		traits = {  politically_connected }
		id = 3158
		skill = 1
		attack_skill = 1
		defense_skill = 1
		planning_skill = 2
		logistics_skill = 1
    }
    
    create_corps_commander = {
		name = "Gustaw Orlicz-Dreszer"
		picture = "M_Gustaw_Orlicz_Dreszer.tga"
		traits = {  politically_connected }
		id = 3159
		skill = 1
		attack_skill = 2
		defense_skill = 1
		planning_skill = 2
		logistics_skill = 2
    }

    create_corps_commander = {
		name = "Antoni Szyling"
		picture = "M_Antoni_Szyling.tga"
		traits = { inflexible_strategist old_guard trickster }
		id = 3160
		skill = 4
		attack_skill = 2
		defense_skill = 5
		planning_skill = 4
		logistics_skill = 2
    }
    
    create_corps_commander = {
		name = "Wiktor Thomée"
		picture = "M_Wiktor_Thomee.tga"
		traits = {  harsh_leader organizer commando }
		id = 3161
		skill = 4
		attack_skill = 2
		defense_skill = 5
		planning_skill = 3
		logistics_skill = 3
    }
    
    create_corps_commander = {
		name = "Franciszek Kleeberg"
		picture = "M_Franciszek_Kleeberg.tga"
		traits = {  infantry_officer infantry_leader swamp_fox }
		id = 3162
		skill = 3
		attack_skill = 3
		defense_skill = 4
		planning_skill = 3
		logistics_skill = 4
    }
    
    create_corps_commander = {
		name = "Tadeusz Kutrzeba"
		picture = "M_Tadeusz_Kutrzeba.tga"
		traits = {  brilliant_strategist career_officer skilled_staffer trait_engineer }
		id = 3163
		skill = 3
		attack_skill = 2
		defense_skill = 2
		planning_skill = 4
		logistics_skill = 4
    }
    
    create_corps_commander = {
		name = "Władysław Bortnowski"
		picture = "M_Wladyslaw_Bortnowski.tga"
		traits = {  media_personality politically_connected organizer }
		id = 3164
		skill = 2
		attack_skill = 3
		defense_skill = 1
		planning_skill = 2
		logistics_skill = 2
    }
    
    create_corps_commander = {
		name = "Janusz Gąsiorowski"
		picture = "M_Janusz_Gasiorowski.tga"
		traits = {  trickster }
		id = 3165
		skill = 2
		attack_skill = 1
		defense_skill = 2
		planning_skill = 3
		logistics_skill = 2
	}

	create_corps_commander = {
		name = "Władysław Anders"
		gfx = "M_Wladyslaw_Anders.tga"
		traits = {  media_personality cavalry_leader war_hero }
		id = 3166
		skill = 2
		attack_skill = 4
		defense_skill = 1
		planning_skill = 2
		logistics_skill = 1
    }

    create_corps_commander = {
		name = "Wilhelm Orlik-Rückemann"
		picture = "M_Ruckemann.tga"
		traits = {  panzer_leader commando }
		id = 3167
		skill = 3
		attack_skill = 2
		defense_skill = 3
		planning_skill = 3
		logistics_skill = 3
    }
    
    create_corps_commander = {
		name = "Emil Krukowicz-Przedrzymirski"
		picture = "M_Krukowicz.tga"
		traits = {  organizer inflexible_strategist }
		id = 3168
		skill = 2
		attack_skill = 1
		defense_skill = 2
		planning_skill = 3
		logistics_skill = 2
    }

    create_corps_commander = {
		name = "Mieczysław Boruta-Spiechowicz"
		picture = "M_Mieczyslaw_Boruta_Spiechowicz.tga"
		traits = {  trait_mountaineer }
		id = 3169
		skill = 2
		attack_skill = 2
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}

}	
#########################################################################
# Poland - 1936
#########################################################################
1936.1.1 = {
	oob = "POL_1936"
	#######################
	# Politics
	#######################
	set_politics = {		
		ruling_party = authoritarian
		last_election = "1935.9.15"
		election_frequency = 36
		elections_allowed = yes
	}
	set_popularities = {
		fascist = 2
		authoritarian = 70
		democratic = 20
		socialist = 1
		communist = 7
	}	
	add_ideas = {
        # Spirit
        # Cabinet
		POL_HoG_Marian_ZyndramKoscialkowski
		POL_FM_Jozef_Beck
        POL_AM_Eugeniusz_Kwiatkowski
        POL_MoS_Wladyslaw_Raczkiewicz
		# POL_HoI_Janusz_Jagrym_Maleszewski
        # Military Staff
		POL_CoStaff_Waclaw_Stachiewicz
		POL_CoArmy_Edward_RydzSmigly
        POL_CoNavy_Jerzy_Swirski
        POL_CoAir_Ludomil_Rayski
	}
	remove_ideas = {
		POL_Nowela_Sierpniowa
	}
	#######################
	# Leaders
	#######################
	# Fascism
	create_country_leader = {
		name = "Boleslaw Piasecki"
		desc = ""
		picture = "P_F_Bolesaw_Piasecki.tga"
		expire = "1965.1.1"
		ideology = falangism
		traits = { POSITION_President SUBIDEOLOGY_Falangism HoS_Barking_Buffoon }
	}
	#######################
	# Generals
	#######################
	remove_unit_leader = 3150
}	
#######################
# Variants
#######################
create_equipment_variant = {
	name = "PZL P.24"
	type = Fighter_equipment_1933
	upgrades = {
		plane_gun_upgrade = 3
		plane_range_upgrade = 0  
		plane_engine_upgrade = 1
		plane_reliability_upgrade = 3
	}
}
#########################################################################
# Poland - 1939
#########################################################################
1939.9.1 = {
    oob = "POL_1939"
    #######################
	# Politics
	#######################
	set_politics = {		
		ruling_party = authoritarian
		last_election = "1938.9.15"
		election_frequency = 36
		elections_allowed = yes
	}
	set_popularities = {
		fascist = 2
		authoritarian = 70
		democratic = 20
		socialist = 1
		communist = 7
	}
	add_ideas = {
		# Laws and Policies
		limited_exports
		one_year_service
		partial_economic_mobilisation
		# Cabinet
		POL_HoG_Felicjan_SlawojSkladkowski
		POL_FM_Jozef_Beck
        POL_AM_Eugeniusz_Kwiatkowski
        POL_MoS_Felicjan_SlawojSkladkowski
		POL_HoI_Kordian_Zamorski
		# Military Staff
		POL_CoStaff_Waclaw_Stachiewicz
		POL_CoArmy_Edward_RydzSmigly
        POL_CoNavy_Jerzy_Swirski
        POL_CoAir_Ludomil_Rayski
	}
	remove_ideas = {
		POL_Nowela_Sierpniowa
	}
	#######################
	# Leaders
	#######################
	# Democracy
	create_country_leader = {
		name = "Tadeusz Bielecki"
		desc = ""
		picture = "P_D_Tadeusz_Bielecki.tga"
		expire = "1965.1.1"
		ideology = social_conservatism
		traits = { POSITION_President SUBIDEOLOGY_Conservatism HoS_Weary_Stiff_Neck }
	}
	# Socialism
	create_country_leader = {
		name = "Kazimierz Puzak"
		desc = ""
		picture = "P_S_Kazimierz_Puzak.tga"
		expire = "1965.1.1"
		ideology = socialism
		traits = { POSITION_General_Secretary SUBIDEOLOGY_Socialism HoS_Popular_Figurehead }
	}
    #######################
	# Generals
	#######################
	remove_unit_leader = 3159
	remove_unit_leader = 3153
  
    create_corps_commander = {
		name = "Roman Abraham"
		picture = "M_Roman_Abraham.tga"
		traits = { brilliant_strategist cavalry_officer cavalry_leader trickster }
		id = 3170
		skill = 4
		attack_skill = 4
		defense_skill = 4
		planning_skill = 3
		logistics_skill = 4
    }
    
    create_corps_commander = {
		name = "Mikołaj Bołtuć"
		picture = "M_Mikolaj_Boltuc.tga"
		traits = {  trait_reckless ranger }
		id = 3171
		skill = 2
		attack_skill = 3
		defense_skill = 3
		planning_skill = 2
		logistics_skill = 1
    }
    
    create_corps_commander = {
		name = "Zygmunt Podhorski"
		picture = "M_Zygmunt_Podhorski.tga"
		traits = {  cavalry_officer organizer swamp_fox }
		id = 3172
		skill = 3
		attack_skill = 2
		defense_skill = 3
		planning_skill = 2
		logistics_skill = 3
    }
    
    create_corps_commander = {
		name = "Wincenty Kowalski"
		picture = "M_Wincenty_Kowalski.tga"
		traits = {  infantry_leader trickster }
		id = 3173
		skill = 3
		attack_skill = 3
		defense_skill = 3
		planning_skill = 3
		logistics_skill = 3
	}
}
# Poland 1940
1940.5.10 = {
	oob = "POL_1940"
}
###################
# Poland - 1946
###################
1946.1.1 = {
	oob = "POL_1944"

	#######################
	# Politics
	#######################
	set_cosmetic_tag = POL_SOV_1946
	set_politics = {		
		ruling_party = communist
		last_election = "1935.9.15"
		election_frequency = 36
		elections_allowed = no
	}
	set_popularities = {
		fascist = 0
		authoritarian = 0
		democratic = 0
		socialist = 0
		communist = 100
	}	
}

###################
# Poland - 1944
###################
1944.6.20 = {
	oob = "POL_1944"
}
###################
# Poland - 1950
###################
1950.1.1 = {

	#######################
	# Politics
	#######################
	set_cosmetic_tag = POL_SOV
	
}
