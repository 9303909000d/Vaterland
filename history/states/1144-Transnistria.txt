state={
	id=1144
	name="STATE_1144"
	
	history={
		owner = UKR
		victory_points = {
			13676 2
		}
		buildings = {
			infrastructure = 3

		}
		add_core_of = UKR
		add_core_of = UKR
		add_core_of = PMR
		1941.6.22 = {
			add_core_of = MOL
			remove_core_of = UKR

		}
		1942.11.22 = {
			controller = ROM

		}
		1944.6.20 = {
			controller = UKR

		}
		2000.1.1 = {
			owner = PMR
			remove_core_of = UKR

		}
    }
	
	provinces={
		13676 13677 13678 
	}
	manpower=469000
	buildings_max_level_factor=1.000
	state_category=rural
}
