
state={
	id=805
	name="STATE_805"

	history={
		owner = BLR
		victory_points = {
			3320 5 
		}
		buildings = {
			infrastructure = 5
			industrial_complex = 1
			air_base = 5

		}
		add_core_of = BLR
		add_core_of = LIT
		1940.5.10 = {
			owner = BLR
			controller = BLR

		}
		1942.11.21 = {
			owner = ROS
			controller = ROS

		}
		1944.6.20 = {
			owner = BLR
			controller = GER

		}
		1946.1.1 = {
			owner = BLR
			add_core_of = BLR
			remove_core_of = BLR
			set_state_name = Vilnius
			set_province_name = {
				id = 3320
				name = Vilnius

			}

		}
		2000.1.1 = {
			owner = LIT
			remove_core_of = BLR

		}

	}

	provinces={
		3320 9274 9295 11342 
	}
	manpower=487000
	buildings_max_level_factor=1.000
	state_category=rural
}
