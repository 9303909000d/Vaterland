state={
	id=664
	name="STATE_664"
	manpower = 854300
	resources={
		steel=13 # was: 20
		chromium = 3 # was: 4
	}
	
	state_category = town
	
	history={
		owner = HUN
		victory_points = {
			6573 3
		}
		buildings = {
			infrastructure = 6
			industrial_complex = 3
		}
		add_core_of = HUN
		add_core_of = SLO
		1939.9.1 = {
			owner = HUN
			add_core_of = HUN
			
		}
		1946.1.1 = {
			owner = HUN
			remove_core_of = HUN
			
		}
		2000.1.1 = {
			owner = SLO
			remove_core_of = HUN
			
		}		
	}
	
	provinces={
		3565 3716 6561 6573 9537 11679 
	}
}
