
state={
	id=194
	name="STATE_194"

	history={
		owner = BLR
		victory_points = {
			6232 2
		}
		buildings = {
			infrastructure = 3
			air_base = 3

		}
		add_core_of = BLR
		add_core_of = BLR
		1942.11.21 = {
			owner = RUK
			controller = RUK

		}
		1944.6.20 = {
			GER = {
				set_province_controller = 215
				set_province_controller = 3203
				set_province_controller = 3293
				set_province_controller = 6319
				set_province_controller = 9554

			}

		}
		1946.1.1 = {
			owner = BLR
			controller = BLR

		}
		2000.1.1 = {
			owner = BLR

		}

	}

	provinces={
		215 3203 3293 3507 6232 6319 6556 9249 9554 11313 11453 13736 
	}
	manpower=934632
	buildings_max_level_factor=1.000
	state_category=rural
}
