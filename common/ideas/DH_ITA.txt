ideas = {


	country = {
		ITA_victory_in_africa = {

			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			picture = ITA_Victory_In_Africa

			modifier = {
				political_power_gain = 0.010
				stability_factor = 0.05
			}
		}

		ITA_weak_goverment = {

			allowed = {
				always = no
				}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			picture = ITA_Weak_Government

			modifier = {
				political_power_cost = 0.05
				send_volunteers_tension = 0.6
				drift_defence_factor = -0.50
				military_leader_cost_factor = 0.45
			}
		}

  	ITA_socialism_unrest = {

			allowed = {
				always = no
			}

			allowed_civil_war = {
					always = yes
			}

			removal_cost = -1

			picture = ITA_Radical_Unrest

			modifier = {
				industrial_capactity_factor = -0.10
				stability_factor = -0.10
				socialist_drift = 0.05
				fascist_drift = 0.05
			}
		}

		ITA_siccita = {

			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			picture =ITA_Siccita
			modifier = {
				population_factor = -0.5
				stability_factor = -0.15
				monthly_population = -0.10
			}
		}

		ITA_economic_depression = {

			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = yes
			}

			removal_cost = -1

			picture = ITA_Great_Depression

			modifier = {
				consumer_goods_factor = 0.25
				stability_factor = -0.25
				trade_opinion_factor = -0.05
				political_power_cost = 0.05
				industrial_capacity_factory = -0.15	
				local_resources_factor = -0.10
				industrial_capacity_dockyard = -0.10
				research_speed_factor = -0.05
				local_building_slots_factor = -0.05
				money_expenses = -2
			}
		}

		ITA_army_mutalia = {

			allowed = {
				always = no
			}

			allowed_civil_war = {
				always = no
			}

			removal_cost = -1

			picture = ITA_Bad_Military

			modifier = {
				war_support_factor = -0.29
				ai_focus_aggressive_factor = -0.50
				training_time_factor = 0.20
				max_planning = -0.20
				experience_gain_army_factor = -0.15
				surrender_limit = -0.20
				encryption_factor = -0.10
				decryption_factor = -0.05
				max_planning = -0.25
				planning_speed = -0.15
				partisan_effect = -0.15
				dig_in_speed = -20
				no_supply_grace = -3
				
			}
		}
}
