state={
	id=1170
	name="STATE_1170"
	
	history={
		owner = UKR
		victory_points = {
	        3483 2
		}
		buildings = {
			infrastructure = 4
			industrial_complex = 1

		}
		add_core_of = POL
		add_core_of = UKR
		1940.5.10 = {
			owner = UKR
			controller = UKR
			set_state_name = Ternopil
			set_province_name = { id = 3482 name = "Ternopil" }

		}
		1942.11.21 = {
			owner = GGP
			controller = GGP

		}
		1944.6.20 = {
			controller = UKR

		}
		1946.1.1 = {
			owner = UKR
			controller = UKR

		}
		2000.1.1 = {
			owner = UKR

		}

	}
	
	provinces={
		438 491 3483 3562 9454 11427 
	}
	manpower=1600406
	buildings_max_level_factor=1.000
	state_category=town
}
