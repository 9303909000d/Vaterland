
state={
	id=184
	name="STATE_184"
	resources={
		chromium=11.000
	}

	history={
		owner = BUL
		victory_points = {
			11774 1
		}
		buildings = {
			infrastructure = 4

		}
		add_core_of = GRE
		add_claim_by = BUL
		1941.6.22 = {
			owner = BUL
			controller = BUL

		}
		1946.1.1 = {
			owner = GRE
			controller = GRE

		}

	}

	provinces={
		11774 13646 
	}
	manpower=382500
	buildings_max_level_factor=1.000
	state_category=rural
}
